//
//  Pokemon.swift
//  Pokedex
//
//  Created by Grant Schulte on 5/6/17.
//  Copyright © 2017 Grant Schulte. All rights reserved.
//

import UIKit

class Pokemon : NSObject {
    var id: Int
    var name: String
    var imageURL: URL?
    var image: UIImage?
    var type: [String]
    var height: Float
    var weight: Float
    var eggDistancePokemonGo: String
    var spawnChancePokemonGo: Double
    var weaknesses: [String]
    var evolutionTree: [Dictionary<String,String>]  // [ {"001", "Bulbasaur"}, {"002", "Ivysaur"}, {"003", "Venusaur"} ]
    
    override init() {
        self.id = 0
        self.name = ""
        self.imageURL = nil
        self.image = nil
        self.type = []
        self.height = 0
        self.weight = 0
        self.eggDistancePokemonGo = ""
        self.spawnChancePokemonGo = 0
        self.weaknesses = []
        self.evolutionTree = []
    }
    
    
    init(ID id: Int, Name name: String, ImageURL imageURL: String, TypeArray type: [String], Height height: String, Weight weight: String, EggHatchDistance eggDistancePokemonGo: String, SpawnChance spawnChancePokemonGo: Double, WeaknessesArray weaknesses: [String], EvolutionTreeStringArray_ID_Name evolutionTree: [Dictionary<String, String>]) {
        self.id = id
        self.name = name
        self.imageURL = URL(string: imageURL)
        // TO SPEED UP SCROLLING, USE WHAT IS WRITTEN HERE. TO SPEED UP LOADING, DON'T KEEP UIIMAGES IN POKEMON OBJECTS -- LOAD FROM URL INSTEAD DURING EACH CELL's ACCESS TO THE IMAGE 
        /*
        if let imgURL = self.imageURL {
            if let imgDATA = try? Data(contentsOf: imgURL) {
                if let img = UIImage(data: imgDATA) {
                    self.image = img
                }
            }
        }
         */
        
        self.image = nil  // Nil tells table to pull the image from URL in tableView
        self.type = type
        let numericalHeight: String = height.substring(to: height.characters.index(height.endIndex, offsetBy: -2))
        self.height = Float(numericalHeight)!
        let numericalWeight: String = weight.substring(to: height.characters.index(height.endIndex, offsetBy: -3))  // "1.23" without " kg"
        self.weight = Float(numericalWeight)!
        self.eggDistancePokemonGo = eggDistancePokemonGo
        self.spawnChancePokemonGo = spawnChancePokemonGo
        self.weaknesses = weaknesses
        self.evolutionTree = evolutionTree
    }
    
}

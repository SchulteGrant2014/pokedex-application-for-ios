//
//  PokemonDetailsViewController.swift
//  Pokedex
//
//  Created by Grant Schulte on 5/7/17.
//  Copyright © 2017 Grant Schulte. All rights reserved.
//

import UIKit

class PokemonDetailsViewController: UIViewController {
    
    @IBOutlet weak var navigationBar: UINavigationItem!
    @IBOutlet weak var pokemonInfo_Image: UIImageView!
    @IBOutlet weak var pokemonImageBackdrop: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var eggDistanceLabel: UILabel!
    @IBOutlet weak var spawnChanceLabel: UILabel!
    @IBOutlet weak var weaknessesLabel: UILabel!
    @IBOutlet weak var evolutionTreeLabel: UILabel!
    
    var pokemon: Pokemon = Pokemon()
    var image: UIImage = UIImage()  // Store image separately so that we have somewhere to put the image upon segue (note: the pokemonInfo_image view isn't constructed until the view loads)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if let backdropURL = URL(string: "http://img03.deviantart.net/49cf/i/2011/068/a/c/pokeball_desktop_icon_by_beccerberry-d3b98cf.png"){
            if let backdropDATA = try? Data(contentsOf: backdropURL) {
                if let pkmImg = UIImage(data: backdropDATA) {
                    pokemonImageBackdrop.image = pkmImg
                }
            }
        }
        
        pokemonInfo_Image.image = image
        
        nameLabel.text = "Name:   \(pokemon.name)"
        var id = "ID#:   "
        if (pokemon.id < 10) {
            id += "00\(pokemon.id)"
        } else if (pokemon.id < 100) {
            id += "0\(pokemon.id)"
        } else {
            id += "\(pokemon.id)"
        }
        idLabel.text = id
        var type: String = "Type:   "
        for i in pokemon.type {
            type += "\(i), "
        }
        typeLabel.text = type.substring(to: type.characters.index(type.endIndex, offsetBy: -2))
        heightLabel.text = "Height:   \(pokemon.height) m"
        weightLabel.text = "Weight:   \(pokemon.weight) kg"
        eggDistanceLabel.text = "Egg Distance (Pokémon Go):   \(pokemon.eggDistancePokemonGo)"
        spawnChanceLabel.text = "Spawn Chance (Pokémon Go):   \(pokemon.spawnChancePokemonGo)"
        var weaknesses: String = "Weaknesses:   "
        for i in pokemon.weaknesses {
            weaknesses += "\(i), "
        }
        weaknessesLabel.text = weaknesses.substring(to: weaknesses.characters.index(weaknesses.endIndex, offsetBy: -2))
        var evols: String = "Evolution Tree:\n"
        for i in pokemon.evolutionTree {
            evols += "\(i["num"]!) - \(i["name"]!),\n"
        }
        evolutionTreeLabel.text = evols.substring(to: evols.characters.index(evols.endIndex, offsetBy: -2))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//
//  ViewController.swift
//  Pokedex
//
//  Created by Grant Schulte on 5/5/17.
//  Copyright © 2017 Grant Schulte. All rights reserved.
//

import UIKit

class PokedexViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var pokedexTableView: UITableView!  // Note: Set up delegate and datasource in viewDidLoad()!
    
    var pokemon: [Pokemon] = []  // Holds all Pokemon to be placed in the Pokedex
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Set up Table View to use itself as the delegate + dataSource
        pokedexTableView.delegate = self
        pokedexTableView.dataSource = self
        
        
        // Store the Pokemon from the JSON file!
        let json = getJSON_asSterilizedObject()
        pokemon = convertJSON_toPokemon(json)
        
    }
    
    /* ####################### Begin JSON file loading functions... ####################### */
    
    func getJSON_asSterilizedObject() -> [AnyObject] {
        
        if let json_filePath = Bundle.main.path(forResource: "pokedex", ofType: "json") {  // If "pokedex.json" file exists, return its path as a String? (an optional)
            if let theDATA = try? Data(contentsOf: URL(fileURLWithPath: json_filePath)) {  // If data loads correctly, continue packaging the data into a JSON object
                do {
                    let json = try JSONSerialization.jsonObject(with: theDATA, options: []) as! [String: AnyObject]
                    return json["pokemon"] as! [AnyObject]  // Access the JSON data using the key-value pair specified by the key "pokemon"
                } catch let e as NSError {
                    print("Error encountered when loading JSON file (\"pokedex.json\").")
                    print(e.description)
                }
            }
        }
        
        return []  // If something goes wrong when loading JSON, return an empty array
        
    }
    
    
    func convertJSON_toPokemon(_ jsonDATA: [AnyObject]) -> [Pokemon] {
        
        // Store list of Pokemon encoded by the JSON file
        var allPkm: [Pokemon] = []
        
        
        // Store the individual data elements of each Pokemon
        var id: Int = 0
        var name: String = ""
        var imageURL: String = ""
        var type: [String] = []
        var height: String = "0 m"
        var weight: String = "0 kg"
        var eggDistancePokemonGo: String = "0 km"
        var spawnChancePokemonGo: Double = 0
        var weaknesses: [String] = []
        var evolutionTree: [Dictionary<String, String>] = []
        
        for i in jsonDATA {  // For each Pokemon in the Pokedex...
            
            if let id_num = i["id"] as? Int {
                id = id_num
            }
            if let pkmName = i["name"] as? String {
                name = pkmName
            }
            if let url = i["img"] as? String {
                imageURL = url
            }
            if let typeList = i["type"] as? [String] {
                for j in typeList {
                    type.append(j)
                }
            }
            if let pkmHeight = i["height"] as? String {
                height = pkmHeight
            }
            if let pkmWeight = i["weight"] as? String {
                weight = pkmWeight
            }
            if let egg = i["egg"] as? String {
                eggDistancePokemonGo = egg
            }
            if let spawn = i["spawn_chance"] as? Double {
                spawnChancePokemonGo = spawn
            }
            if let weaknessList = i["weaknesses"] as? [String] {
                weaknesses = weaknessList
            }
            
            
            // ---------------------- Begin Parsing the JSON for EVOLUTION TREE data... ---------------------- */
            
            // Get a list of the Pokemon in the evolution tree...:
            if let prev_evolutions = i["prev_evolution"] as? [Dictionary<String, String>] {  // First, get a list of the previous evolutions
                for j in prev_evolutions {  // Go through that list analyzing individual Pokemon...
                    if let num = j["num"] {  // If the Pokemon has an ID number specified...
                        if let name = j["name"] {  // And it also has a name specified...
                            var pkmEvol = Dictionary<String, String>()
                            pkmEvol["num"] = num
                            pkmEvol["name"] = name
                            evolutionTree.append(pkmEvol)  // Add that evolution's data to the list!
                        }
                    }
                }
            }
            
            if let num = i["num"] as? String {  // Next, add the current Pokemon to the list
                if let name = i["name"] as? String {
                    var pkmEvol = Dictionary<String, String>()
                    pkmEvol["num"] = num
                    pkmEvol["name"] = name
                    evolutionTree.append(pkmEvol)
                }
            }
            
            if let next_evolutions = i["next_evolution"] as? [Dictionary<String, String>] {  // Finally, get a list of the next evolutions
                for j in next_evolutions {  // Go through that list analyzing individual Pokemon...
                    if let num = j["num"] {  // If the Pokemon has an ID number specified...
                        if let name = j["name"] {  // And it also has a name specified...
                            var pkmEvol = Dictionary<String, String>()
                            pkmEvol["num"] = num
                            pkmEvol["name"] = name
                            evolutionTree.append(pkmEvol)  // Add that evolution's data to the list!
                        }
                    }
                }
            }
            
            // ---------------------- End Parsing the JSON for EVOLUTION TREE data... ---------------------- */
            
            // Create a new Pokemon out of all the data...
            let newPkm = Pokemon(ID: id, Name: name, ImageURL: imageURL, TypeArray: type, Height: height, Weight: weight, EggHatchDistance: eggDistancePokemonGo , SpawnChance: spawnChancePokemonGo, WeaknessesArray: weaknesses, EvolutionTreeStringArray_ID_Name: evolutionTree)
            
            allPkm.append(newPkm)
            
            // Reset values to base values for the next Pokemon...
            id = 0
            name = ""
            imageURL = ""
            type = []
            height = "0 m"
            weight = "0 kg"
            eggDistancePokemonGo = "0 km"
            spawnChancePokemonGo = 0
            weaknesses = []
            evolutionTree = []
        }
        
        
        return allPkm
    }
    
    
    
    /* ####################### End JSON file loading functions... ####################### */
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // How many sections is the table divided into?
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // How many rows are there in each section of the table?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemon.count
    }
    
    // What should each cell look like/contain? (Creates the actual cells based off the cell object returned by this function)
    // Uses "tableView.dequeueReusableCellWithIdentifier(...)" to construct cells based off a template that is already made in the storyboard file
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Create a cell to store the Pokemon's information
        let cell = pokedexTableView.dequeueReusableCell(withIdentifier: "pokemonCell", for: indexPath)
        
        // Set the Pokemon's name as the main title parameter
        var numID: String = ""
        if (pokemon[indexPath.row].id < 10) {
            numID = "00\(pokemon[indexPath.row].id)"
        } else if (pokemon[indexPath.row].id < 100) {
            numID = "0\(pokemon[indexPath.row].id)"
        } else {
            numID = "\(pokemon[indexPath.row].id)"
        }
        cell.textLabel?.text = "\(numID) - \(pokemon[indexPath.row].name)"  // <--------------------- Cell Title (1/3)
        
        // Set the Pokemon's type to be the subtitle parameter
        var typeDescription = ""
        let typesArray = pokemon[indexPath.row].type
        var i_begin = 0
        let i_end = typesArray.count
        for type in typesArray {
            typeDescription += type
            if (i_begin < i_end - 1) {
                typeDescription += ", "
            }
            i_begin += 1
        }
        cell.detailTextLabel?.text = typeDescription  // <--------------------- Cell Subtitle (2/3)
        
        
        
        // Set the Pokemon's image to be the image located at the provided URL by loading it IF IT HASN'T BEEN LOADED ALREADY
        
        // First, load the local placeholder image on the main thread...
        cell.imageView?.image = UIImage(named: "pokeball.png")
        
        // Then, switch to a background thread to smooth the experience. Attempt to load the real Pokemon image from URL only IF NOT YET LOADED
        if pokemon[indexPath.row].image == nil {
            
            DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                if let imageURL = self.pokemon[indexPath.row].imageURL {  // Optional - must use "if let" or unwrap imageURL with Bam Operator (!) later
                    if let imageDATA = try? Data(contentsOf: imageURL) {
                        let img = UIImage(data: imageDATA)
                        self.pokemon[indexPath.row].image = img
                    }
                }
                
                // Back in main thread, update the AI
                DispatchQueue.main.async {
                    cell.imageView?.image = self.pokemon[indexPath.row].image  // <--------------------- Cell Image (3/3)
                }
            }
            
        } else {
            
            cell.imageView?.image = pokemon[indexPath.row].image;
            
        }
        
        /* Error Fix: [Source = http://stackoverflow.com/questions/31254725/transport-security-has-blocked-a-cleartext-http/32560433#32560433]
        Error: App Transport Security has blocked a cleartext HTTP (http://) resource load since it is insecure. Temporary exceptions can be configured via your app's Info.plist file.
        
        --> This is a security precaution preventing us from opening up unsecure "http://" URLs. Only "https://" URLs are accessible.
        --> Get around this using the following modifications to "info.plist":

        Fix by modifying "info.plist":
            1) Add a new row, select "App Transport Security Settings" (or name it "NSAppTransportSecurity")
            2) Add a sub-element "NSExceptionDomains". Change the type to Dictionary so that it can be expanded into sub-elements, too.
            3) Add a sub-sub-element "MyDomainNameExample.com"
            4) Add sub-sub-sub-elements "NSIncludesSubdomains" and "NSTemporaryExceptionAllowsInsecureHTTPLoads" of type Boolean - set it to YES.
        */
        

        return cell
    }
    
    // Add headers to each section in the table
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Pokémon"
        case 1:
            return "Items"
        default:
            return "SECTION HEADER - I ADDED THIS IN SWITCH-DEFAULT"
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pokemonCellClicked" {
            if let pkmIndex = pokedexTableView.indexPathForSelectedRow?.row {
                let pkm = pokemon[pkmIndex]
                if let destinationVC = segue.destination as? PokemonDetailsViewController {
                    destinationVC.pokemon = pkm
                    destinationVC.navigationBar.title = pkm.name
                    
                    if pkm.image != nil {
                        destinationVC.image = pkm.image!
                    }
                }
            }
        }
        if let selectedRowIndex = pokedexTableView.indexPathForSelectedRow {
            pokedexTableView.deselectRow(at: selectedRowIndex, animated: true)
        }
    }
    
    

}


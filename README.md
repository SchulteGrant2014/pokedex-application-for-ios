# POKEDEX iOS - README #

This project is a Pokedex mobile application for iOS written in Swift. It is a personal project to practice using table views in Swift. This project was created as a personal follow-up project to an iOS development workshop series hosted by UCLA ACM (computer science club at UCLA) called HackCamp.

The Pokedex pulls images from the internet (www.serebii.net) of all the Pokemon in the Pokedex. Upon booting up the app, these images are downloaded and the interface is set up while the user views the launch screen. When all images have been downloaded, the launch screen changes to the main storyboard, which contains the table of all Pokemon in Generation 1. Each cell of the table can be clicked to bring the user to a detailed information page for the specific Pokemon in that cell.

The reason for downloading all of the images at once before the user is able to access the main view, as opposed to loading them progressively as the user scrolls through the table, is that having the images pre-loaded allows for a smoother experience when scrolling.

The data for each Pokemon is stored in a JSON file: the URL of the Pokemon image on serebii.net, as well as the specific Pokemon's statistics. This data is loaded into each Pokemon object.

Below are some sample images of the application:

## Loading Screen: ##

![Simulator Screen Shot May 12, 2017, 12.45.27 AM.png](https://bitbucket.org/repo/KrrxxXr/images/4290308205-Simulator%20Screen%20Shot%20May%2012,%202017,%2012.45.27%20AM.png)

## Pokedex Table View (Main Storyboard): ##

![Simulator Screen Shot May 12, 2017, 12.45.42 AM.png](https://bitbucket.org/repo/KrrxxXr/images/1182640452-Simulator%20Screen%20Shot%20May%2012,%202017,%2012.45.42%20AM.png)

## Pokemon Detailed Info View: ##

![Simulator Screen Shot May 12, 2017, 12.48.16 AM.png](https://bitbucket.org/repo/KrrxxXr/images/1489544312-Simulator%20Screen%20Shot%20May%2012,%202017,%2012.48.16%20AM.png)